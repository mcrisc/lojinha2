package edu.ifsp.lojinha2.web.infra;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.context.WebContext;

public class ProcessadorTemplate {
	public static void processar(
			String templateName, 
			HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		
		final ServletContext servletContext = request.getServletContext();
		IWebContext ctx = new WebContext(request, response, servletContext);
		TemplateEngine engine = TemplateManager.getEngine(servletContext);
		response.setContentType("text/html;charset=UTF-8");
		engine.process(templateName, ctx, response.getWriter());		
	}
}
