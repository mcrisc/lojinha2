package edu.ifsp.lojinha2.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ifsp.lojinha2.modelo.Cliente;
import edu.ifsp.lojinha2.modelo.Usuario;
import edu.ifsp.lojinha2.persistencia.ClienteDAO;
import edu.ifsp.lojinha2.persistencia.PersistenceException;
import edu.ifsp.lojinha2.web.infra.ProcessadorTemplate;
import edu.ifsp.lojinha2.web.infra.URIUtils;

@WebServlet("/cadastrar/*")
public class CadastroServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long id = URIUtils.extractId(request);
		ClienteDAO dao = new ClienteDAO();
		Cliente cliente = null;
		if (id != -1) {
			try {
				cliente = dao.findById(id);
			} catch (PersistenceException e) {
				throw new ServletException(e);
			}
		}

		if (cliente == null) {
			cliente = new Cliente();
		}
		request.setAttribute("cliente", cliente);		
		ProcessadorTemplate.processar(
				"cadastro", request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String username = request.getParameter("usuario");
		String senha = request.getParameter("senha");
		Usuario usuario = new Usuario();
		usuario.setUsername(username);
		usuario.setPassword(senha);

		long id = URIUtils.extractId(request);
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		Cliente cliente = new Cliente();
		cliente.setId(id);
		cliente.setNome(nome);
		cliente.setEmail(email);
		cliente.setUsuario(usuario);
				
		ClienteDAO dao = new ClienteDAO();
	
		try {
			dao.save(cliente);
		} catch (PersistenceException e) {
			throw new ServletException(e);
		}			
		
		URIUtils.relativeRedirect(request, response, "/cadastrar/%d".formatted(cliente.getId()));
	}

}
