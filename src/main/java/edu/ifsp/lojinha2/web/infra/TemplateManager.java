package edu.ifsp.lojinha2.web.infra;

import javax.servlet.ServletContext;

import org.thymeleaf.TemplateEngine;

public class TemplateManager {
	private TemplateManager() {}

	public static TemplateEngine getEngine(ServletContext servletContext) {
		TemplateEngine engine = (TemplateEngine)servletContext
				.getAttribute("templateEngine");
		
		if (engine == null) {
			throw new IllegalStateException("TemplateEngine é nulo");			
		}
		
		return engine;
	}
	
	
}
