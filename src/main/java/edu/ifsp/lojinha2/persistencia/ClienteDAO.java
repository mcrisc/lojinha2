package edu.ifsp.lojinha2.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import edu.ifsp.lojinha2.modelo.Cliente;
import edu.ifsp.lojinha2.modelo.Usuario;

public class ClienteDAO {

	public Cliente save(Cliente cliente) throws PersistenceException {
		
		try (Connection conn = DatabaseConnector.getConnection()) {
			/* Iniciando a transação */
			conn.setAutoCommit(false);

			try {
				if (cliente.getId() == -1) {
					insert(conn, cliente);	
				} else {
					update(conn, cliente);
				}
			} catch (SQLException | PersistenceException e) {
				/* Rollback, se ocorrer alguma exceção nos métodos `insert()` ou `update()` */
				conn.rollback();
				throw e;
			}
			
			/* Encerrando a transação */
			conn.commit();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}	
		
		return cliente;
	}


	private void insert(Connection conn, Cliente cliente) throws PersistenceException, SQLException {
		PreparedStatement ps = conn.prepareStatement(
				"INSERT INTO usuario (username, password) VALUES (?, ?)", 
				Statement.RETURN_GENERATED_KEYS);
		Usuario usuario = cliente.getUsuario();
		ps.setString(1, usuario.getUsername());
		ps.setString(2, usuario.getPassword());
		
		ps.executeUpdate();
		
		ResultSet rs = ps.getGeneratedKeys();
		if (rs.next()) {
			usuario.setId(rs.getLong(1));
		} else {
			throw new PersistenceException("Chave faltando: Usuario.id");					
		}
		rs.close();
		ps.close();
						
		ps = conn.prepareStatement(
				"INSERT INTO cliente (nome, email, usuario) VALUES (?, ?, ?)", 
				Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, cliente.getNome());
		ps.setString(2, cliente.getEmail());
		ps.setLong(3, usuario.getId());
		ps.executeUpdate();
		rs = ps.getGeneratedKeys();
		if (rs.next()) {
			cliente.setId(rs.getLong(1));
		} else {
			throw new PersistenceException("Chave faltando: Cliente.id");					
		}
		rs.close();
		ps.close();
	}

	private void update(Connection conn, Cliente cliente) throws SQLException {
			PreparedStatement ps = conn.prepareStatement(
					"UPDATE cliente SET nome = ?, email = ? WHERE id = ?");
			ps.setString(1,  cliente.getNome());
			ps.setString(2, cliente.getEmail());
			ps.setLong(3, cliente.getId());
			ps.executeUpdate();
			ps.close();
			
			final Usuario usuario = cliente.getUsuario();
			if (usuario != null && usuario.getId() != -1) {
				ps = conn.prepareStatement(
						"UPDATE usuario SET username = ? WHERE id = ?");
				ps.setString(1, usuario.getUsername());
				ps.setLong(2, usuario.getId());
				ps.executeUpdate();
				ps.close();				
			}			
	}

	
	public List<Cliente> listAll() throws PersistenceException {
		List<Cliente> clientes = new ArrayList<>();

		try (Connection conn = DatabaseConnector.getConnection()) {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(
					"SELECT c.id, c.nome, c.email, u.username "
					+ "FROM cliente c, usuario u "
					+ "WHERE c.usuario = u.id");
			while (rs.next()) {
				Cliente c = mapRow(rs);
				clientes.add(c);
			}			
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		
		return clientes;
	}

	public Cliente findById(Long id) throws PersistenceException {
		Cliente cliente = null;		
		try (Connection conn = DatabaseConnector.getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					"SELECT c.id, c.nome, c.email, u.username "
					+ "FROM cliente c, usuario u "
					+ "WHERE c.usuario = u.id AND c.id = ?");
			ps.setLong(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				cliente = mapRow(rs);
			}			
			rs.close();
			ps.close();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
		return cliente;
	}


	private Cliente mapRow(ResultSet rs) throws SQLException {
		Cliente cliente;
		cliente = new Cliente();
		cliente.setId(rs.getLong("id"));
		cliente.setNome(rs.getString("nome"));
		cliente.setEmail(rs.getString("email"));
		cliente.getUsuario().setUsername(rs.getString("username"));
		return cliente;
	}
}







