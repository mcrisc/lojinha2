DROP TABLE IF EXISTS cliente;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS produto;


CREATE TABLE usuario (
	id INTEGER GENERATED ALWAYS AS IDENTITY,
	username VARCHAR(20) NOT NULL,
	password VARCHAR(40) NOT NULL
);
ALTER TABLE usuario ADD CONSTRAINT pk_usuario PRIMARY KEY (id);


CREATE TABLE produto (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
	descricao VARCHAR(100) NOT NULL,
	preco DECIMAL(8, 2) NOT NULL
);
ALTER TABLE produto ADD CONSTRAINT pk_produto PRIMARY KEY (id);


CREATE TABLE cliente (
    id INTEGER GENERATED ALWAYS AS IDENTITY,
	nome VARCHAR(40) NOT NULL,
	email VARCHAR(40),
	usuario BIGINT REFERENCES usuario(id) ON DELETE CASCADE
);
ALTER TABLE cliente ADD CONSTRAINT pk_cliente PRIMARY KEY (id);

