package edu.ifsp.lojinha2.modelo;

public class Usuario {
	private long id = -1;
	private String username;
	private String password;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @deprecated Será removido em breve.
	 */
	public String getNome() {
		return username;
	}

}
