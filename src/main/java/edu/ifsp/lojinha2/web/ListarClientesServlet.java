package edu.ifsp.lojinha2.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.ifsp.lojinha2.modelo.Cliente;
import edu.ifsp.lojinha2.persistencia.ClienteDAO;
import edu.ifsp.lojinha2.persistencia.PersistenceException;
import edu.ifsp.lojinha2.web.infra.ProcessadorTemplate;

@WebServlet("/listar")
public class ListarClientesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			ClienteDAO dao = new ClienteDAO();
			List<Cliente> clientes = dao.listAll();
			request.setAttribute("clientes", clientes);
			ProcessadorTemplate.processar(
					"listar-clientes", request, response);
			
		} catch (PersistenceException e) {
			e.printStackTrace();
		}

	}


}
