package edu.ifsp.lojinha2.web.infra;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class URIUtils {

	public static long extractId(HttpServletRequest request) {
		String uri = request.getRequestURI();
		Matcher matcher = Pattern.compile("/(\\d+)/?$").matcher(uri);
		long id = -1L;
		if (matcher.find()) {
			id = Long.parseLong(matcher.group(1));
		}
		return id;
	}
	
	public static void relativeRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
		String ctxPath = request.getContextPath();
		if (url.startsWith("/")) {
			url = url.substring(1);
		}
		String location = ctxPath + "/" + url;
		response.sendRedirect(location);
	}
}
