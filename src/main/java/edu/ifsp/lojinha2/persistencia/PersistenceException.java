package edu.ifsp.lojinha2.persistencia;

public class PersistenceException extends Exception {
	
	public PersistenceException(Throwable cause) {
		super(cause);
	}
	
	public PersistenceException(String message) {
		super(message);
	}
}
